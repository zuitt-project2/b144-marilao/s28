/*3. Create a fetch request using the GET method that will retrieve all the to do list items from JSON Placeholder API.*/

fetch('https://jsonplaceholder.typicode.com/todos',{
	method: 'GET'
})
.then(res => res.json())
.then(toDos => console.log(toDos))

/*4. Using the data retrieved, create an array using the map method to return just the title of every item and print the result in the console.*/


/*fetch("https://jsonplaceholder.typicode.com/todos")
	.then(res => res.json())
	.then(data => {
		let titles = data.map((title) => title.title);

		console.log(titles);
	});*/
/*let myTitle = list.map(function(title){
	return `${list.title}`
});

console.log(list.title)*/


/*5. Create a fetch request using the GET method that will retrieve a single to do list item from JSON Placeholder API.
6. Using the data retrieved, print a message in the console that will provide the title and status of the to do list item.*/
fetch('https://jsonplaceholder.typicode.com/todos/1')
.then(response => response.json())
.then(data => {
	console.log(data)
})
/*7. Create a fetch request using the POST method that will create a to do list item using the JSON Placeholder API.*/
fetch('https://jsonplaceholder.typicode.com/todos', {
	method: 'POST',
	headers: {
		'Content-Type': 'application/json'
	},
	
	body: JSON.stringify({
		userId: '4',
	    id: 143,
	    title: "My new to do list",
	    completed: false
	})
})
.then(res => res.json())
.then(data => console.log(data))

/*8. Create a fetch request using the PUT method that will update a to do list item using the JSON Placeholder API.*/
fetch('https://jsonplaceholder.typicode.com/todos/1', {
    method: 'PUT',
    headers: {
        'Content-Type': 'application/json'
    },
    body: JSON.stringify({
        userId: '4',
	    id: 134,
	    title: "My new to do list revised",
	    completed: true
    })
})
.then(response => response.json())
.then(data => console.log(data))


/*9. Update a to do list item by changing the data structure to contain the following properties:
- Title
- Description
- Status
- Date Completed
- User ID*/
fetch('https://jsonplaceholder.typicode.com/todos/1', {
    method: 'PUT',
    headers: {
        'Content-Type': 'application/json'
    },
    body: JSON.stringify({
        title:'My new list of items',
		description:'this is my revised list of items',
		status:'active',
		dateCompleted:'12/3/2021',
		userID:'34'
    })
})
.then(response => response.json())
.then(data => console.log(data))

/*10. Create a fetch request using the PATCH method that will update a to do list item using the JSON Placeholder API.*/
fetch('https://jsonplaceholder.typicode.com/todos/1', {
    method: 'PATCH',
    headers: {
        'Content-Type': 'application/json'
    },
    body: JSON.stringify({
        title:'My revised list of items',
		
    })
})
.then(response => response.json())
.then(data => console.log(data))

/*12. Create a fetch request using the DELETE method that will delete an item using the JSON Placeholder API.*/
fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'DELETE'
})
.then(res=> res.json())
.then(data=>console.log(data))


