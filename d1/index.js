//JAVASCRIPT SYNCHRONOUS VS ASYNCHRONOUS

//SYNCHRONOUS JAVASCRIPT
//In synchronous operations tasks are performed one at a time and only when one is completed, the following is unblocked. 
//Javascript is by default is synchronous
console.log('Hello World')
/*console.log(Hello Again)*/

/*for(let i = 0; i <=1500; i++){
	console.log(i);
}*/
//blocking is when the execution of additional js process must wait untile the operation completes. 
//blocking is just the code is slow
console.log('Goodbye')

//Asynchronous Javascript

//We can classify most asynchronous js operations with 2 primary triggers.

//1. Browser API/WEB API events or functions. Theses include methods like setTimeout, or event handlers like onclicke, mouse over, scroll and many more


/*function printMe(){
	console.log('print Me');
}

setTimeout(printMe, 5000)*/

//another sample

/*function print(){
	console.log('print meeeee')
}

function test(){
	console.log('test')
}

setTimeout(print, 4000)
test()*/

//another example
/*function f1(){
	console.log('f1')
}

function f2(){
	console.log('f2')
}

function main(){
	console.log('main')

	setTimeout(f1, 0)

	new Promise((resolve, reject) => 
		resolve('I am a promise')
		).then(resolve => console.log(resolve))

	f2()
}

main();*/
//CALLBACK QUEUE (Data Structure)
//in javascript, we have a queue of callback functions. It is called a callback queue or task queue. A queue data structure is "First-In-First-Out".


//JOB QUEUE
//Every time a promise occurs in the code, the executor function gets into the job queue. The event loop works as usual , to look into the queues but gives priority to the job queue items over the callback queue items when the stack is free.


//2. Promises. A unique javascript object that allows us to perform asynchronous

//The promises object represents the eventual completion (or failure) of an asynchronous operation and its resulting value
/*
new Promise((resolve, reject) => 
		resolve('I am a promise')
	).then(resolve => console.log(resolve))*/


//REST API using a jsonplaceholder

//Getting all posts (GET method)


//Fetch -> allows us to asynchronously request for a resource(data)
//we used the promise for async

/*
Syntax: 

fetch('url', {optional objects})
.then(response => response.json())
.then(data)

Optional object - which contains additional information about our requests such the method, the body, and the headers of our request.
*/

console.log(fetch('https://jsonplaceholder.typicode.com/posts'))
//A promise may be in one of 3 possible states: pending, fulfilled or rejected.

fetch('https://jsonplaceholder.typicode.com/posts')
.then(res => res.json())//parse the response as JSON
.then(result => {
	console.log(result)//process the results
})

//async await function
async function fetchData(){
	//wait for the 'Fetch' method to complete then stores the value in a variable name
	let result = await fetch('https://jsonplaceholder.typicode.com/posts')
	console.log(result)
	console.log(typeof result)

	//converts the data from the 'response'
	let json = await result.json()
	console.log(json)
	console.log(typeof json)
	
}
fetchData();



//Retrieving a specific post
fetch('https://jsonplaceholder.typicode.com/posts/1')
.then(response => response.json()) //parse the response as JSON
.then(data => {
	console.log(data) // process the results
})



//Creates a new post
fetch('https://jsonplaceholder.typicode.com/posts', {
	method: 'POST',
	headers: {
		'Content-Type': 'application/json'
	},
	//Sets the content/body of the 'request' object to be sent to the backend
	body: JSON.stringify({
		title: 'New Post',
		body: 'Hello Again',
		userId: 2
	})
})
.then(res => res.json())
.then(data => console.log(data))


//Updating a post 
fetch('https://jsonplaceholder.typicode.com/posts/1', {
    method: 'PUT',
    headers: {
        'Content-Type': 'application/json'
    },
    body: JSON.stringify({
        body: 'Hello Again',
        userID: 1
    })
})
.then(response => response.json())
.then(data => console.log(data))

//PUT method- updates the whole document. modifying resource where the client sends data that updates the entire resource.

//The Patch method applies partial update to resource


//Delete a post
fetch('https://jsonplaceholder.typicode.com/posts/1', {
	method: 'DELETE'
})
.then(res=> res.json())
.then(data=>console.log(data))


//Filtering posts
//Information sent via the url can be done by adding the question mark symbol (?)
/*
Syntax:
//Individual Parameters
	//'url?parameterName=value'
//Multiple Parameters
	//'url?parameterName=value&paramB=valueB'
*/

fetch('https://jsonplaceholder.typicode.com/posts?userId=1')
.then(res => res.json())
.then(data => console.log(data))




//Retrieve a nested/related comments to posts
//Retrieving comments for a specific post(/posts/:id/comments)

fetch('https://jsonplaceholder.typicode.com/posts/2/comments')
.then(res => res.json())
.then(data => console.log(data))
